# Отчет по изучению **Linux** #
Крицкий Алексей. Изучал Linux в рамках первого задания task001 fpy2019 (с 22 по 26 марта 2019 г.), также сталкивался ранее в течение чуть более года.

## Содержание
+ Дистрибутив Linux
+ Знакомство с OC
+ Использование
+ Работа с консолью
+ Результаты
+ Заключение
+ Материалы
____________________________________________________
### Дистрибутив Linux
В качестве дистрибутива Linux использую Ubuntu 16.04. Операционная система установлена на ноутбук как дополнительная и загружается с внешнего диска (для экономии внутренней памяти ноутбука).  
Использую уже более года.

### Знакомство с OC
Так как ранее работал с Ubuntu, получил общее представление с точки зрения пользователя (файловая система, консоль, прикладные программы).
Прочел про общие идеи устройства, [философию Unix][unix_philosophy].

Прочел частично курс ["Введение в Linux и Bash"][linux_bash].

Также повторил материалы ранее изучаемого курса "Информационные технологии: операционные системы", касающиеся **Linux**: работы с **терминалом**, работы с редактором **vi**, скриптового языка **shell**, работы с процессами.

### Использование
Использовал и использую Linux как основную систему для программирования по предмету "Информационные технологии" (изучение операционных систем, технологии разработки web-приложений, программного обеспечения мобильных устройств), выполнения курсовой работы и всего, что касается данного факультатива.

В соответствии с этими целями установлен следующий набор прикладных программ:
* Программирование:
 - **atom** и дополнительные пакеты - Markdown, мощный общий редактор программного кода
 - **Eclipse** (и Java соответственно) и дополнительные пакеты - в основном для Java и С++ (в Windows привык к Visual Studio, которого нет для Linux), но главное удобство в наличии множества дополнительных плагинов
 - **IDLE** - для обучения Python
 - **PyCharm** - как продвинутая IDE для Python
 - **IntelliJ Idea** - на мой взгляд, более комфортабильный для Java, нежели Eclipse
 - **Android Studio** - для разработки мобильных приложений для андроид, а также способ установки *Android AVD* и *Android SDK*
 - **WebStorm** - для веб-приложений js-html-css, но использую в основном для разработки кроссплатформенных мобильных приложений на *Ionic*
 - **Ionic Framework** - для разработки кроссплатформенных мобильных приложений
 - **Visual Studio Code** - как легкий редактор кода С//С++ и не только
* Интернет:
 - **Google Chrome** - для интернет-серфинга и разработки веб-приложений
 - **Веб-браузер Firefox** - также для интернет-серфинга и разработки веб-приложений, отличается наличием большего числа плагинов
* Взаимодействие:
 - **Telegram** - как мессенджер "без лишнего"
 - **Skype** - возможность  видеочатов и видеоконференций (может понадобиться в перспективе в любой момент), однако пока не использовал

### Работа с консолью
Повторил материалы ранее изучаемого курса _"Информационные технологии: операционные системы"_, касающиеся Linux и командной строки.
Также начал читать [_"Командная строка Linux. Полное руководство" Шоттс У_][command_prompt]. (сейчас остановился в середине 4-ой главы).
____________________________________________________

### Результаты
1. Установил дистрибутив Ubuntu 16.04 в качестве дополнительной системы на рабочем компьютере
1. Ознакомился в общем с Linux
2. Начал использовать Ubuntu как основную систему для программирования


### Заключение
Проделана работа по изучению Linux, что открывает возможности для широкого использования данной ОС в дальнейшем
____________________________________________________

### Материалы
+ Философия Unix <https://ru.wikipedia.org/wiki/%D0%A4%D0%B8%D0%BB%D0%BE%D1%81%D0%BE%D1%84%D0%B8%D1%8F_Unix>
+ Введение в Linux и Bash - <https://younglinux.info/linux>
+ "Командная строка Linux. Полное руководство", Шоттс У. - <http://linuxcommand.org/tlcl.php>
+ "Современные операционные системы", Эндрю Таненбаум - <https://freedocs.xyz/pdf-454357206>

[unix_philosophy]: https://ru.wikipedia.org/wiki/%D0%A4%D0%B8%D0%BB%D0%BE%D1%81%D0%BE%D1%84%D0%B8%D1%8F_Unix
[linux_bash]: https://younglinux.info/linux
[command_prompt]: http://linuxcommand.org/tlcl.php​
