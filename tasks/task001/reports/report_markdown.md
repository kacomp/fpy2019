# Отчет про изучение **Markdown** #
Крицкий Алексей. Изучал Markdown в рамках первого задания task001 fpy2019 (с 22 по 25 марта 2019 г.)

## Содержание
+ Порядок знакомства
 - Общее знакомство
 - Изучение синтаксиса Markdown
 - Конвертирование в разные форматы
 - Дополнительные инструменты
+ Практика
 - Воспроизведение примеров и конвертирование
 - Оформление данного отчета
 - Оформление других отчетов
+ Результаты
+ Заключение
+ Материалы
____________________________________________________

### Порядок знакомства
* Общее знакомство - статья wikipedia по Markdown с примерами:
<https://en.wikipedia.org/wiki/Markdown>
<https://ru.wikipedia.org/wiki/Markdown>
* Изучение синтаксиса Markdown:
<https://belousovv.ru/markdown_syntax>
Проверяя собственноручно в [файле][hello.md].
* Конвертирование в разные форматы. Рассмотрев разные редакторы, остановился на *atom*.
Установил и использовал далее дополнительные пакеты для работы с *Markdown* в *atom*:
  + Markdown Preview (был установлен) - отображение Markdown в HTML
  + markdown-to-htmlОформление отчетов  - конвертация в *HTML*
  + markdown-pdf  -  конвертация в *pdf*
  Конвертировал [файл c Markdown-разметкой][hello.md]  в [html][hello.html] и [pdf][hello.pdf].
* Дополнительные инструменты. Как дополнительный инструмент для работы с *Markdown* в *atom* установил и ознакомился с пакетом [Markdown Writer](https://atom.io/packages/markdown-writer).

### Практика
* Воспроизведение примеров из вышеперечисленных источников в [файле][hello.md] и конвертация в [html][hello.html] и [pdf][hello.pdf].
* Оформление данного отчета про изучение *Markdown* с помощью *Markdown*.
* Оформление отчетов про изучение *Linux* и *Git* с помощью *Markdown*.
  Также оформил с помощью *Markdown* ранее сделанный [*отчет о результатах работы над курсовым проектом за 20-26 февраля 2019*][report].
____________________________________________________

### Результаты
- Изучил и поработал с **Markdown**
- Настроил редактор **atom** для удобной работы с *Markdown*
- Составил **отчеты** с помощью *Markdown*

### Заключение
1. В достаточно короткие сроки изучил **Markdown** и попрактиковался в его использовании.
1. Составил данный **отчет** и еще два отчета с помощью *Markdown*.
1. **Собираюсь в дальнейшем** использовать данную технологию для оформления отчетов и других документов.
____________________________________________________

### Материалы:
1. [Markdown в Wiki](https://en.wikipedia.org/wiki/Markdown) - англоязычная версия
[Markdown в Wiki](https://ru.wikipedia.org/wiki/Markdown) - русскоязычная версия
2. <https://belousovv.ru/markdown_syntax> - Синтаксис Markdown
3. Пакеты atom для работы с Markdown:
 - [Markdown Writer](https://atom.io/packages/markdown-writer)
 - [markdown-to-html](https://atom.io/packages/markdown-to-html)
 - [markdown-pdf](https://atom.io/packages/markdown-pdf)
 - [Markdown Preview](https://atom.io/packages/markdown-preview)

[hello.md]: /../markdown_examples/hello.md
[hello.html]: /../markdown_examples/hello.html
[hello.pdf]: /../markdown_examples/hello.pdf
[report]: /report_course_work_260219/report.html
