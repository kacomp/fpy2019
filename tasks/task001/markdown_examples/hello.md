# Header 2
### Header 3

~~crossed out text~~

**Strong bold**    
*bold*

`Inline code`

`public static int`

* marked list element

- non-marked list element

+ any-marked list element

1. numbered list element


Alternetive first level header syntax
=====================================


Alternative second level header syntax
--------------------------------------

> Blockquote

[Markdown](https://markdown.com "Markdown")

![Alt-text](logo.png "Ionic")

<table>
<tr>
<td>Milk</td>
<td>Tank</td>
</tr>
</table>

T&T &copy; 567 < 5787

Some fist paragraph
Some second paragraph

*Some other block*

> **Good idea**
> > 1. *Learn Git*
> > 1. *Learn Markdown*
> > > + ###### To HTML  
> > > + ###### To PDF  

> > 1. *Learn Linux*
>
> **Bad idea**
> > * ~~Learn nothing~~

1. I go swimming every Friday

  But i don't like swimming pool

2. That all.

1954\. was very sad

21. *Code*:

    int count;
    float mass;

3. *End*.

    int main() {

    }

___

_hih_

***

__hah__

***************

*Heh*

---------------
