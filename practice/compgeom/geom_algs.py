from math import sqrt


class Vector2D:
    def __init__(self, x=0.0, y=0.0):
        self.x = x
        self.y = y

    def __add__(self, other):
        return Vector2D(self.x + other.x, self.y + other.y)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        return self

    def __neg__(self):
        return Vector2D(-self.x, -self.y)

    def __sub__(self, other):
        return self + (-other)

    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y
        return self

    def __matmul__(self, other):
        return self.x * other.x + self.y * other.y

    def __mul__(self, scalar):
        return Vector2D(self.x * scalar, self.y * scalar)

    def __len__(self):
        return abs(sqrt(self @ self))

    def __abs__(self):
        return len(self)

    def __bool__(self):
        return self.x != 0 or self.y != 0

    def __repr__(self):
        return 'Vector2D({}, {})'.format(self.x, self.y)

    def __str__(self):
        return '({}, {})'.format(self.x, self.y)


class Vector3D(Vector2D):
    def __init__(self, x=0.0, y=0.0, z=0.0):
        super().__init__(x, y)
        self.z = z

    def __add__(self, other):
        return Vector3D(self.x + other.x, self.y + other.y, self.z + other.z)

    def __iadd__(self, other):
        self.x += other.x
        self.y += other.y
        self.z += other.z
        return self

    def __neg__(self):
        return Vector3D(-self.x, -self.y, -self.z)

    def __sub__(self, other):
        return self + (-other)

    def __isub__(self, other):
        self.x -= other.x
        self.y -= other.y
        self.z -= other.z
        return self

    def __matmul__(self, other):
        return self.x * other.x + self.y * other.y + self.z * other.z

    def __mul__(self, scalar):
        return Vector3D(self.x * scalar, self.y * scalar, self.z * scalar)

    def __bool__(self):
        return self.x != 0 or self.y != 0 or self.z != 0

    def __repr__(self):
        return 'Vector3D({}, {}, {})'.format(self.x, self.y, self.z)

    def __str__(self):
        return '({}, {}, {})'.format(self.x, self.y, self.z)


class Quaternion:
    def __init__(self, x0=0.0, x1=0.0, x2=0.0, x3=0.0):
        self.x0 = x0
        self.x1 = x1
        self.x2 = x2
        self.x3 = x3

    def __add__(self, other):
        return Quaternion(self.x0 + other.x0, self.x1 + other.x1, self.x2 + other.x2, self.x3 + other.x3)

    def __iadd__(self, other):
        self.x0 += other.x0
        self.x1 += other.x1
        self.x2 += other.x2
        self.x3 += other.x3
        return self

    def __neg__(self):
        return Quaternion(-self.x0, -self.x1, -self.x2, -self.x3)

    def __invert__(self):
        return Quaternion(self.x0, -self.x1, -self.x2, -self.x3)

    def __sub__(self, other):
        return self + (-other)

    def __isub__(self, other):
        self.x0 -= other.x0
        self.x1 -= other.x1
        self.x2 -= other.x2
        self.x3 -= other.x3
        return self

    def __matmul__(self, other):
        return Quaternion(self.x0 * other.x0 - self.x1 * other.x1 - self.x2 * other.x2 - self.x3 * other.x3,
                          self.x0 * other.x1 + self.x1 * other.x0 + self.x2 * other.x3 - self.x3 * other.x2,
                          self.x0 * other.x2 + self.x2 * other.x0 - self.x1 * other.x3 + self.x3 * other.x1,
                          self.x0 * other.x3 + self.x3 * other.x0 + self.x1 * other.x2 - self.x2 * other.x1)

    def __mul__(self, scalar):
        return Quaternion(self.x0 * scalar, self.x1 * scalar, self.x2 * scalar, self.x3 * scalar)

    def __and__(self, other):
        return self.x0 * other.x0 + self.x1 * other.x1 + self.x2 * other.x2 + self.x3 * other.x3

    def __len__(self):
        return abs(self & self)

    def __abs__(self):
        return len(self)

    def __bool__(self):
        return self.x0 != 0 or self.x1 != 0 or self.x2 != 0 or self.x3 != 0

    def __repr__(self):
        return 'Quaternion({}, {}, {}, {})'.format(self.x0, self.x1, self.x2, self.x3)

    def __str__(self):
        return '({}, {}, {}, {})'.format(self.x0, self.x1, self.x2, self.x3)


class Stack:
    def __init__(self):
        self.items = []

    def push(self, item):
        self.items.append(item)

    def pop(self):
        return self.items.pop()

    def is_empty(self):
        return self.items == []


def rel_point2line(p0: Vector2D, p1: Vector2D, p2: Vector2D):
    """Defines relation point p0 to the directional line (p1, p2) in the plane."""
    return (p2.x - p1.x) * (p0.y - p1.y) - (p0.x - p1.x) * (p2.y - p1.y)


def line_segments_is_intersect(p1: Vector2D, p2: Vector2D, p3: Vector2D, p4: Vector2D):
    """Detects intersection of line segments (p1, p2) and (p3, p4) in the plane"""
    d1 = rel_point2line(p1, p3, p4)
    d2 = rel_point2line(p2, p3, p4)
    d3 = rel_point2line(p3, p1, p2)
    d4 = rel_point2line(p4, p1, p2)

    if d1 == 0 and d2 == 0 and d3 == 0 and d4 == 0:
        if (p3 - p1) * (p4 - p1) <= 0 or (p3 - p2) * (p4 - p2) <= 0 or (p1 - p3) * (p2 - p3) <= 0 or (p1 - p4) * (
                p2 - p4) <= 0:
            return True
        else:
            return False
    else:
        if d1 * d2 <= 0 and d3 * d4 <= 0:
            return True
        else:
            return False


def is_simple_polygon(polygon: list):
    """Detects the simple polygon"""
    length = len(polygon)
    for i in range(0, length - 3):
        for j in range(i + 2, length - 1):
            if line_segments_is_intersect(polygon[i],
                                          polygon[i + 1],
                                          polygon[j],
                                          polygon[j + 1]):
                return False

    for i in range(1, length - 2):
        if line_segments_is_intersect(polygon[i],
                                      polygon[i + 1],
                                      polygon[length - 1],
                                      polygon[0]):
            return False

    return True


def is_convex_polygon(polygon: list):
    """Detect the convex polygon"""
    if not is_simple_polygon(polygon):
        return False
    j = 0
    orientation = 0
    length = len(polygon)
    for i in range(0, length - 2):
        orientation = rel_point2line(polygon[j], polygon[j + 1], polygon[j + 2])
        if orientation < 0:
            orientation = -1
            break
        if orientation > 0:
            orientation = 1
            break

    for i in range(j + 1, length - 2):
        if rel_point2line(polygon[i], polygon[i + 1], polygon[i + 2]) * orientation < 0:
            return False

    if rel_point2line(polygon[length - 2], polygon[length - 1], polygon[0]) * orientation < 0 \
            or rel_point2line(polygon[length - 1], polygon[0], polygon[1]) * orientation < 0:
        return False

    return True
