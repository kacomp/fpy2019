import random
from tkinter import *

from geom_algs import Vector2D


class CanvasWidget:
    def __init__(self, name, width, height, background="#000000"):
        self.WIDTH = width
        self.HEIGHT = height
        self.name = name
        self.objects = dict()
        self.situations_counter = 0

        self.window = Tk()
        self.window.title(name)

        self.canvas = Canvas(self.window, width=self.WIDTH, height=self.HEIGHT, background=background)
        self.canvas.pack()

        self.create_situation()

    def create_circle(self, point: Vector2D, radius, fill: str):
        half_radius = radius / 2
        return self.canvas.create_oval(point.x - half_radius,
                                       point.y - half_radius,
                                       point.x + half_radius,
                                       point.y + half_radius,
                                       fill=fill)

    def print_answer(self, answer, color='white', font_size=20, font_style="Arial"):
        self.canvas.create_text(self.WIDTH / 2,
                                self.HEIGHT - font_size,
                                text=answer,
                                font=font_style + " " + str(font_size),
                                fill=color)

    def create_situation(self):
        pass

    def move_animation(self):
        pass

    def change_situation(self):
        self.canvas.delete('all')
        self.create_situation()

    def rand_vector2d(self):
        return Vector2D(random.random() * self.WIDTH, random.random() * self.HEIGHT)
