from geom_algs import line_segments_is_intersect
from widget import CanvasWidget


def create_situation(self):
    # Given
    self.p1 = self.rand_vector2d()
    self.p2 = self.rand_vector2d()
    self.p3 = self.rand_vector2d()
    self.p4 = self.rand_vector2d()

    self.line = self.canvas.create_line(self.p1.x, self.p1.y, self.p2.x, self.p2.y, fill="blue")
    self.line = self.canvas.create_line(self.p3.x, self.p3.y, self.p4.x, self.p4.y, fill="red")

    # Solution
    if line_segments_is_intersect(self.p1, self.p2, self.p3, self.p4):
        answer = "Lines intersect"
    else:
        answer = "Lines don't intersect"
    self.print_answer(answer)


CanvasWidget.create_situation = create_situation
widget = CanvasWidget('rel_line2line', 500, 500)


def keyrelease_handler(event):
    if event.keysym == "Return":
        widget.change_situation()

    elif event.keysym == "Escape":
        exit(0)


def main():
    widget.canvas.focus_set()
    widget.canvas.bind("<KeyRelease>", keyrelease_handler)
    widget.window.mainloop()


main()
