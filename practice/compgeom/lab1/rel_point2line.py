from geom_algs import rel_point2line
from widget import CanvasWidget


def create_situation(self):
    # Given
    self.p0 = self.rand_vector2d()
    self.p1 = self.rand_vector2d()
    self.p2 = self.rand_vector2d()

    self.line = self.canvas.create_line(self.p1.x, self.p1.y, self.p2.x, self.p2.y, fill="green")
    self.point0 = self.create_circle(self.p0, 5, "yellow")
    self.point1 = self.create_circle(self.p1, 5, "blue")
    self.point2 = self.create_circle(self.p2, 5, "red")

    # Solution
    result = rel_point2line(self.p0, self.p1, self.p2)
    if result == 0:
        answer = 'On the line'
    elif result < 0:
        answer = 'Left'
    else:
        answer = 'Right'
    self.print_answer(answer)


CanvasWidget.create_situation = create_situation
widget = CanvasWidget('rel_point2line', 500, 500)


def keyrelease_handler(event):
    if event.keysym == "Return":
        widget.change_situation()

    elif event.keysym == "Escape":
        exit(0)


def main():
    widget.canvas.focus_set()
    widget.canvas.bind("<KeyRelease>", keyrelease_handler)
    widget.window.mainloop()


main()
