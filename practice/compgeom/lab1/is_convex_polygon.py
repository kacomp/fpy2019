import random

from geom_algs import is_convex_polygon
from widget import CanvasWidget

MIN_VERTEXES_NUM = 4
MAX_VERTEXES_NUM = 6


def create_situation(self):
    # Given
    vertexes_num = random.randrange(MIN_VERTEXES_NUM, MAX_VERTEXES_NUM + 1, 1)
    polygon = []
    for i in range(0, vertexes_num):
        polygon.append(self.rand_vector2d())

    for i in range(0, vertexes_num):
        i_next = (i + 1) % vertexes_num
        self.canvas.create_line(polygon[i].x,
                                polygon[i].y,
                                polygon[i_next].x,
                                polygon[i_next].y, fill="green")

    # Solution
    if is_convex_polygon(polygon):
        answer = 'Is a convex'
    else:
        answer = 'Is not a convex'
    self.print_answer(answer, "blue")


CanvasWidget.create_situation = create_situation
widget = CanvasWidget('Convex polygon', 500, 500, "#FFFFFF")


def keyrelease_handler(event):
    if event.keysym == "Return":
        widget.change_situation()

    elif event.keysym == "Escape":
        exit(0)


def main():
    widget.canvas.focus_set()
    widget.canvas.bind("<KeyRelease>", keyrelease_handler)
    widget.window.mainloop()


main()
