from math import sin, cos

from geom_algs import Vector3D, Quaternion, Vector2D
from widget import CanvasWidget

WAIT_TIME = 33


def create_situation(self):
    self.vi = Vector3D(1, 0, 0)
    self.vj = Vector3D(0, 1, 0)
    self.vk = Vector3D(0, 0, 1)

    self.vi_l = self.vk
    self.vj_l = self.vj
    self.vk_l = -self.vi

    self.eye = Vector3D(0, 0, 1500)
    self.focus_plane1 = Vector3D(self.WIDTH / 4, self.HEIGHT / 2, 0)
    self.focus_plane2 = Vector3D(self.WIDTH / 4 * 3, self.HEIGHT / 2, 0)

    self.axis = Vector3D(1, 1, 0.2)

    self.a = Vector3D(500, 0, 0)

    self.A = [Vector3D(self.vi_l.x, self.vi_l.y, self.vi_l.z),
              Vector3D(self.vj_l.x, self.vj_l.y, self.vj_l.z),
              Vector3D(self.vk_l.x, self.vk_l.y, self.vk_l.z)]

    angle = 0.05

    # rotating quaternion
    ang_sin = sin(angle / 2)
    ang_cos = cos(angle / 2)
    self.h = Quaternion(ang_cos, self.axis.x * ang_sin, self.axis.y * ang_sin, self.axis.z * ang_sin)

    # Given
    # object (cube)
    edge_size = 80
    self.vertexes = [Vector3D(edge_size, edge_size, edge_size),
                     Vector3D(-edge_size, edge_size, edge_size),
                     Vector3D(edge_size, -edge_size, edge_size),
                     Vector3D(edge_size, edge_size, -edge_size),
                     Vector3D(-edge_size, -edge_size, -edge_size),
                     Vector3D(-edge_size, -edge_size, edge_size),
                     Vector3D(edge_size, -edge_size, -edge_size),
                     Vector3D(-edge_size, edge_size, -edge_size)]
    self.edges = [[1, 2, 3],
                  [5],
                  [6],
                  [7],
                  [5, 6, 7],
                  [2],
                  [3],
                  [1]]

    self.n = len(self.vertexes)

    self.situations_counter += 1


def move_animation(self):
    self.canvas.delete('all')

    # separate line
    self.canvas.create_line(self.WIDTH / 2,
                            0,
                            self.WIDTH / 2,
                            self.HEIGHT, fill="green")

    # Solution
    # rotation via quaternions
    pixels = []
    for i in range(0, self.n):
        s = self.h @ Quaternion(0, self.vertexes[i].x, self.vertexes[i].y, self.vertexes[i].z) @ (~self.h)
        self.vertexes[i] = Vector3D(s.x1, s.x2, s.x3)

    # draw orthogonal projection of vertexes (left part of window)
    for i in range(0, self.n):
        ap = self.vertexes[i] - self.a
        pixels.append(Vector3D(self.A[0] @ ap, self.A[1] @ ap, self.A[2] @ ap) + self.focus_plane1)
        self.create_circle(Vector2D(pixels[i].x, pixels[i].y), 5, "blue")

    # draw central projection of vertexes (right part of window)
    for i in range(0, self.n):
        ap = self.vertexes[i] - self.a
        m = Vector3D(self.A[0] @ ap, self.A[1] @ ap, self.A[2] @ ap)
        cp = self.eye.z - m.z
        pixels.append((m * (self.eye.z / cp)) - (self.eye * (m.z / cp)) + self.focus_plane2)
        self.create_circle(Vector2D(pixels[len(pixels) - 1].x, pixels[len(pixels) - 1].y), 5, "blue")

    # draw edges
    for i in range(0, self.n):
        for j in self.edges[i]:
            self.canvas.create_line(pixels[i].x,
                                    pixels[i].y,
                                    pixels[j].x,
                                    pixels[j].y, fill="red")

            self.canvas.create_line(pixels[self.n + i].x,
                                    pixels[self.n + i].y,
                                    pixels[self.n + j].x,
                                    pixels[self.n + j].y, fill="purple")

    self.print_answer('Situation {}'.format(self.situations_counter), 'black')


CanvasWidget.create_situation = create_situation
CanvasWidget.move_animation = move_animation
widget = CanvasWidget('Projections 3D', 1000, 500, "#FFFFFF")


def keyrelease_handler(event):
    if event.keysym == "Return":
        widget.change_situation()

    elif event.keysym == "Escape":
        exit(0)


def animation():
    widget.move_animation()
    widget.window.after(30, animation)


def main():
    widget.canvas.focus_set()
    widget.canvas.bind("<KeyRelease>", keyrelease_handler)
    animation()


main()

widget.window.mainloop()
